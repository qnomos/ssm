<?php 
require_once('Connections/tools.php');

?>
<?php
	$v_sql = "SELECT *
	FROM Libretto_diario AS ld 
	LEFT JOIN Sedi_scuola AS se ON se.ID_sede_scuola = ld.ID_sede_scuola 
	LEFT JOIN Utenti AS ut ON ut.ID_utente = ld.ID_specializzando_libretto
	
	LEFT JOIN Attivita AS at ON at.ID_attivita = ld.ID_attivita
	LEFT JOIN Tipo_attivita AS ta ON ta.ID_tipo_attivita = ld.ID_tipo_attivita
	LEFT JOIN Attivita_formative AS af ON af.ID_attivita_formativa = ld.ID_attivita_formativa_libretto 
		LEFT JOIN Cat_attivita_formative AS ca ON ca.ID_cat_attivita_formativa = af.ID_cat_attivita_formativa 
		LEFT JOIN Ambiti_disciplinari AS ad ON ad.ID_ambito_disciplinare = af.ID_amb_disciplinare 
		LEFT JOIN Settori_scientifici AS ss ON ss.ID_settore_scientifico = af.ID_settore_scientifico_attivita 
	 WHERE ld.ID_specializzando_libretto = ".$codice_op." AND ld.qta_att != 0 ORDER BY ld.data_libretto_diario DESC, ld.ID_libretto_diario DESC";

$maxRows_ListaAttForm = 8;
$pageNum_ListaAttForm = 0;
if (isset($_GET['pageNum_ListaAttForm'])) {
  $pageNum_ListaAttForm = $_GET['pageNum_ListaAttForm'];
}
$startRow_ListaAttForm = $pageNum_ListaAttForm * $maxRows_ListaAttForm;

mysql_select_db($database_scuoledispecializzazione, $scuoledispecializzazione);
$query_ListaAttForm = $v_sql;
$query_limit_ListaAttForm = sprintf("%s LIMIT %d, %d", $query_ListaAttForm, $startRow_ListaAttForm, $maxRows_ListaAttForm);
$ListaAttForm = mysql_query($query_limit_ListaAttForm, $scuoledispecializzazione) or die(mysql_error());
$row_ListaAttForm = mysql_fetch_assoc($ListaAttForm);

$totalRows_ListaAttForm = mysql_num_rows($ListaAttForm);

if ($totalRows_ListaAttForm > 0){

?>

   <!-- <div class="panel" id="spy10">
        <div class="panel-heading ">
          <i class="icon-graphic"></i>-->
           
        
        <div class="mt20 col-md-12">
             <h3>Ultime registrazioni del Libretto Diario</h3>
            
             <table class="table table-hover mbn">
                                <thead>
                                <tr>
                                  <th><div align="center">Data</div></th>
                                  
                                  <th><div align="center">Sede</div></th>
                                  <?php if($_SESSION['reg_operativo']=="S"){ ?>
                                  <th><div align="center"><?php echo $_SESSION['nome_reg_operativo']; ?></div></th>
                                  <?php } ?>
                                  <th><div align="center">Attività</div></th>
                                    <th><div align="center">Quantità</div></th>
                                    <th><div align="center">Tutor</div></th>
                                  </tr>
                                </thead>
                                <tbody>

                                  <?php 
								 
								  do { 

									  $tipo_sett = $row_ListaAttForm['libretto_diario_tipo_sett']; 
									  switch ($tipo_sett) {
											case 'PROF':
												$profess = " ".$sigla_cfup." ";
												$cl_prof = "primary";
												break;
											case 'NOPR':
												$profess = " ".$sigla_cfnup." ";
												$cl_prof = "info";
												break;
											case 'PNOP':
												$profess = " ".$sigla_cfupnp." ";
												$cl_prof = "warning";
												break;
										}
$id_data_mult = $row_ListaAttForm['ID_libretto_diario'];									  
mysql_select_db($database_scuoledispecializzazione, $scuoledispecializzazione);
$query_date_mult = "SELECT * FROM Libretto_diario WHERE Libretto_diario.date_multiple = ".$id_data_mult;
$date_mult = mysql_query($query_date_mult, $scuoledispecializzazione) or die(mysql_error());
									  ?>
                                  <tr class="<?php echo $cl_prof; ?>">
                                    <td style="font-size:12px;"><?php 
									$data_diario = strtotime($row_ListaAttForm['data_libretto_diario']);
									echo date('d/m/Y', $data_diario); 
									
									while($row_date_mult = mysql_fetch_assoc($date_mult))
									{
										$data_multipla = strtotime($row_date_mult['data_libretto_diario']);
									echo "<br>".date('d/m/Y', $data_multipla);
									}
									
									?></td>
                                   
                                    <td><?php 
									
									echo outputDecode($row_ListaAttForm['nome_sede_scuola']);
									
									if($_SESSION['malattie_infettive']=="N" AND $row_ListaAttForm['tutor_operativo'] !=""){
										$tutor_operativo = outputDecode($row_ListaAttForm['tutor_operativo']);
										echo "<br><em>Tutor Operativo ".$tutor_operativo."</em>";
									}

									 ?></td>
                                     <?php if($_SESSION['reg_operativo']=="S"){ ?>
                                    <td align="left"><?php echo outputDecode($row_ListaAttForm['registro_operativo']); ?>
                                    </td>
                                    <?php } ?>
                                    <td>
									  <?php 
									  
										$amb_disciplinare = outputDecode($row_ListaAttForm['nome_ambito_disciplinare']);
										$sett_scientifico = outputDecode($row_ListaAttForm['desc_settore_scientifico']);
										
										echo $sett_scientifico."<br>".$amb_disciplinare;
									  
									 // echo $row_ListaAttForm['desc_settore_scientifico']; ?><br />
<?php if ($row_ListaAttForm['desc_tipo_attivita'] != "Non applicabile") {
										  
										  echo outputDecode($row_ListaAttForm['desc_tipo_attivita']);
										  } ?> <?php 
										  
										  echo outputDecode($row_ListaAttForm['desc_attivita']); ?></td>
                                      <td align="center">
									  
									  <?php 
									  if ($row_ListaAttForm['att_multipla'] == "S" AND $row_ListaAttForm['qta_att'] > 0) {
										  echo $row_ListaAttForm['qta_att'];
										  } else{
										   echo "1"; 
										  }
										  
										
										   ?>
                                          
                                          </td>
                                      <td align="center">
									  <?php 
									
									  	switch($row_ListaAttForm['ok_tutor_libretto']) {
										case "S":
											echo "<span class=\"fa fa-check-circle-o\" style=\"font-size:16px;\" ></span>";
										break;
										case "N":
											echo "<span class=\"fa fa-minus\" style=\"font-size:16px;\" ></span>"; 
										break;
										default:
											echo "<span class=\"fa fa-circle-o\" style=\"font-size:16px;\" ></span>"; 
										}

									  
									  
									  ?></td>
                                  </tr>
                                    <?php 
									} while ($row_ListaAttForm = mysql_fetch_assoc($ListaAttForm));
								  
									 ?>
                                </tbody>
                            </table>
                            <?php
							 if($_GET['sp'] != "") {
								$url_lista = "home.php?sp=".$_GET['sp']."&a=t"; 
							 }else{
								$url_lista = "libretto_lista.php";
							 }
							?>
          <div align="right" style="font-size:12px;"><a href="<?php echo $url_lista; ?>#libretto"><span class="text-dark">Tutte le Registrazioni ></span></a></div>  
   </div>
   <!-- </div>-->
<?php
}
mysql_free_result($ListaAttForm);

?>
