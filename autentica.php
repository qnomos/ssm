<?php require_once('Connections/scuoledispecializzazione.php'); ?>
<?php 
require_once('Connections/tools.php');
 ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

//session_start();    
//$my_sess = session_id();
//$mynonce = sha1($my_sess);


$caller_nonce = $_POST["nonce"];

refererControl($caller_nonce);


$my_login_specializzandi = "-1";
if (isset($_POST['utente'])) {
	
  $my_login_specializzandi = inputControl($_POST['utente']);
  
}
mysql_select_db($database_scuoledispecializzazione, $scuoledispecializzazione);
$query_specializzandi = sprintf("SELECT * FROM Utenti WHERE mail_utente = %s AND (Utenti.ID_profilo != 1 AND Utenti.ID_profilo != 5 AND Utenti.ID_profilo != 6 AND Utenti.ID_profilo != 9 ) AND Utenti.utente_attivo = 'S' ", GetSQLValueString($my_login_specializzandi, "text"));
$specializzandi = mysql_query($query_specializzandi, $scuoledispecializzazione) or die(mysql_error());
$row_specializzandi = mysql_fetch_assoc($specializzandi);
$totalRows_specializzandi = mysql_num_rows($specializzandi);

if ($totalRows_specializzandi == 0) {
 // echo "Database vuoto!";
  header("location: login.php?e=1");
  exit;
} else {
	
$userPassword = trim($_POST['psw']);

  // avvio un ciclo for che si ripete per il numero di occorrenze trovate
 for ($x = 0; $x < $totalRows_specializzandi; $x++){

    
	$psw_db = $row_specializzandi['psw_hash']; 
	 	if (password_verify($userPassword, $psw_db)) {
	 //echo "Password corretta!";
	  
		$id_specializz = $row_specializzandi['ID_utente']; 
		$id_profilo = $row_specializzandi['ID_profilo']; 
		
		setcookie("cod_profilo", $id_profilo);
		session_start();
		$_SESSION['sds_fr'] = $row_specializzandi['id_sds'];
		if ($id_profilo == 2){
			setcookie("cod_specializz", $id_specializz);
		}else{
			setcookie("cod_td", $id_specializz);	
		}
		
		header("location: home.php");
	$autenticato = "S";
 	exit;
	break;
	  
	} else {
		//echo "Password non corretta!";
	  $autenticato = "N";
	}
	
  } //fine ciclo for
  
   if($autenticato == "N"){
   header("location: login.php?e=1");
   }
}

?>
<?php
mysql_free_result($specializzandi);

mysql_free_result($Generale);
?>
