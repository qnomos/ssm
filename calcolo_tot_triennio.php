<?php require_once('Connections/scuoledispecializzazione.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

session_start();
if(isset($_SESSION['sds_fr']) AND $_SESSION['sds_fr'] != "")
{
	$id_sds = $_SESSION['sds_fr'];
}
elseif(isset($_SESSION['sds']) AND $_SESSION['sds'] != "")
{
	$id_sds = $_SESSION['sds'];
}

mysql_select_db($database_scuoledispecializzazione, $scuoledispecializzazione);
$query_totalizz_list = "SELECT * FROM Totalizzatori WHERE Totalizzatori.id_scuola_tot=$id_sds AND (Totalizzatori.id_programma LIKE '%".$pd2."%') ORDER BY Totalizzatori.ordine_totalizz";
$totalizz_list = mysql_query($query_totalizz_list, $scuoledispecializzazione) or die(mysql_error());
$row_totalizz_list = mysql_fetch_assoc($totalizz_list);
$totalRows_totalizz_list = mysql_num_rows($totalizz_list);


$t_richieste_tri = array();
$filtro_tot = array();
$i=0;

if($totalRows_totalizz_list > 0)
{
	do{
		$id_tot = $row_totalizz_list['ID_totalizz'];
		
		$val_richiesti = $row_totalizz_list['richiesti_tot'];
		array_push($t_richieste_tri, $val_richiesti);
			
		
		$filtri = outputDecode($row_totalizz_list['filtro_totalizz']);
		if($row_totalizz_list['tot_frequenza']=="S"){
				$filtri .= " GROUP BY Libretto_diario.data_libretto_diario HAVING COUNT(Libretto_diario.data_libretto_diario) >0 ";
			}
		array_push($filtro_tot, $filtri);
		$i=$i+1;
		
	 } while ($row_totalizz_list = mysql_fetch_assoc($totalizz_list));
}

 
$tot_perc_precorso_tri = sizeof($t_richieste_tri)*100; 

if($_POST['report'] == 'S')
{
	$sql_tot = "SELECT *
	FROM Libretto_diario 
	LEFT JOIN Sedi_scuola AS se ON se.ID_sede_scuola = Libretto_diario.ID_sede_scuola 
	LEFT JOIN Utenti AS ut ON ut.ID_utente = Libretto_diario.ID_specializzando_libretto
	LEFT JOIN Attivita_formative AS af ON af.ID_attivita_formativa = Libretto_diario.ID_attivita_formativa_libretto 
		LEFT JOIN Cat_attivita_formative AS ca ON ca.ID_cat_attivita_formativa = af.ID_cat_attivita_formativa 
		LEFT JOIN Ambiti_disciplinari AS ad ON ad.ID_ambito_disciplinare = af.ID_amb_disciplinare 
		LEFT JOIN Settori_scientifici AS ss ON ss.ID_settore_scientifico = af.ID_settore_scientifico_attivita 
		LEFT JOIN Tipo_attivita AS ta ON Libretto_diario.ID_tipo_attivita = ta.ID_tipo_attivita 
	 WHERE (Libretto_diario.ID_specializzando_libretto = ".$ID_utente.") AND (Libretto_diario.ok_tutor_libretto = 'S') ";


	if ($v_anno == "X"){
		$filro_tot .= " AND (Libretto_diario.data_libretto_diario BETWEEN '".$d_da."' AND '".$d_a."' ) ";
	}else{
		$filro_tot .= " AND (Libretto_diario.anno_specializzando_libretto = '".$v_anno."' ) ";
	}
	
	if ($cfu != "X"){
		$filro_tot .= " AND (Libretto_diario.libretto_diario_tipo_sett = '".$cfu."') ";
	}
	
	if ($sett_scientifico != "X"){
		$filro_tot .= " AND (ss.ID_settore_scientifico = ".$sett_scientifico.") ";
	}
	
	if ($tipo_att != "X"){
		$filro_tot .= " AND (ta.ID_tipo_attivita = ".$tipo_att.") ";
	}
	for($i=0; $i<sizeof($t_richieste_tri); $i++)
	{		
		mysql_select_db($database_scuoledispecializzazione, $scuoledispecializzazione);
		$query_totalizz[$i] = $sql_tot.$filro_tot.$filtro_tot[$i]." ORDER BY Libretto_diario.ID_attivita";
		$totalizz[$i] = mysql_query($query_totalizz[$i], $scuoledispecializzazione) or die(mysql_error());
		$row_totalizz[$i] = mysql_fetch_assoc($totalizz[$i]);
		$totalRows_totalizz[$i] = mysql_num_rows($totalizz[$i]);
		
	}
		
}else{
	for($i=0; $i<sizeof($t_richieste_tri); $i++)
	{
		mysql_select_db($database_scuoledispecializzazione, $scuoledispecializzazione);
		$query_totalizz[$i] = "SELECT Libretto_diario.qta_att, Libretto_diario.ID_attivita FROM Libretto_diario WHERE Libretto_diario.ID_specializzando_libretto = ".$id_specializzando." AND Libretto_diario.ok_tutor_libretto = 'S' ".$filtro_tot[$i]." ORDER BY Libretto_diario.ID_attivita";
		$totalizz[$i] = mysql_query($query_totalizz[$i], $scuoledispecializzazione) or die(mysql_error());
		$row_totalizz[$i] = mysql_fetch_assoc($totalizz[$i]);
		$totalRows_totalizz[$i] = mysql_num_rows($totalizz[$i]);
		
		
	}
}


for($i=0; $i<sizeof($t_richieste_tri); $i++)
{
	$tot_tri[$i] = 0;
	if($totalRows_totalizz[$i] > 0){
		if(strpos($filtro_tot[$i], "HAVING COUNT(Libretto_diario.data_libretto_diario)") == false){
			do{
				
					$qta_db_att = $row_totalizz[$i]['qta_att'];
					if($qta_db_att == ""){
						$qta_att = 1;
					}else{
						$qta_att = $qta_db_att;
					}
					$tot_tri[$i] += $qta_att;
		
			}while ($row_totalizz[$i] = mysql_fetch_assoc($totalizz[$i]));
		}else{
			$tot_tri[$i] = $totalRows_totalizz[$i];
		}
	}
}

$totale_perc_tri = 0;
$richiesti_tri = 0;
for($i=0; $i<sizeof($t_richieste_tri); $i++)
{
	$tot_perc_tri[$i] = ($tot_tri[$i]*100)/$t_richieste_tri[$i];
	if ($tot_perc_tri[$i] > 100){ $tot_perc_tri[$i] = 100; }
	$tot_perc_tri[$i] = number_format($tot_perc_tri[$i], 1);
	if ($tot_tri[$i]>=$t_richieste_tri[$i]){ $ttot_tri[$i]=$t_richieste_tri[$i]; }else{$ttot_tri[$i]=$tot_tri[$i];};
	if($tot_tri[$i] > $t_richieste_tri[$i])
	{
		$qta_attivita_tri = $t_richieste_tri[$i];
	}
	else
	{
		$qta_attivita_tri = $tot_tri[$i];
	}
	$richiesti_tri += $t_richieste_tri[$i]; 
	$totale_perc_tri += $qta_attivita_tri;
}